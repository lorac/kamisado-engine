package ai

import k "gitlab.com/lorac/kamisado-engine/kamisado"
import "math"

type Ai struct {
	kamisado *k.Kamisado
}

func NewAiPlayer(kami *k.Kamisado) (player *Ai) {
	return &Ai{kamisado: kami}

}

func (player *Ai) negamax(depth int, alpha int, beta int, color int) int {
	if depth == 0 || player.(*kamisado).isCompleted() {
		return color // * the heuristic value of node
	}
	// Generate possible moves
	bestValue := -math.MinInt32
	// for each move calculate negamax and keep best bestValue
	// for move := range possibleMoves
	// 		v := -negamax(move, depth -1, -beta, -alpha, -color)
	//		bestValue := max( bestValue, v)
	//		alpha := max(alpha, v)
	//		if alpha >= beta
	//			break
	return bestValue
}
