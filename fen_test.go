package kamisado

import "testing"
import "fmt"

func TestGivenDefaultFen_ThenExpect(t *testing.T) {
	var b *Board
	b = parseFEN(DefaultFen)
	b.turn = Black
	b.moveNumber = 1
	if fen := b.generateFen(); fen != DefaultFen {
		t.Errorf(fmt.Sprintf("Expecting %s, got %s", DefaultFen, fen))
	}
}
