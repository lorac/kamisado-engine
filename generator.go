package kamisado

var (
	nextRank int = 16
	nextFile int = 1

	moveUp        = -nextRank
	moveDown      = nextRank
	moveLeft      = -nextFile
	moveRight     = nextFile
	moveUpLeft    = moveUp + moveLeft
	moveUpRight   = moveUp + moveRight
	moveDownLeft  = moveDown + moveLeft
	moveDownRight = moveDown + moveRight

	whiteDelta = []int{moveDownLeft, moveDown, moveDownRight}
	blackDelta = []int{moveUpLeft, moveUp, moveUpRight}

	whitePieceStartPos int = 1 // rank 2
	blackPieceStartPos int = 6 // rank 7
)

const (
	boardSize int = 128
	size      int = 8
)

// Generator creates possible moves for a given board position
type Generator struct {
	board          *Board
	legalEnding    []bool
	legalDelta     []int8
	lastMoveSquare int
	moves          []Move
	isDone         bool
}

// NewGenerator creates a new generator for a given board
func NewGenerator(board *Board) *Generator {
	g := new(Generator)
	g.board = board

	return g
}

func (g *Generator) createMove(from int, to int) Move {
	move := Move{From: from, To: to}
	move.Content = g.board.data[to]

	// piece of same color on to square
	if g.board.data[to]*g.board.data[from] > 0 {
		move.MovedPiece = Empty
		return move
	}

	move.MovedPiece = g.board.data[from]
	move.From = from
	move.To = to

	return move
}

// check wether a piece can move towards a specific direction
// does not work for knight obviously
func (g *Generator) canMoveTowardDirection(delta []int8, direction int8) bool {
	for _, d := range delta {
		if d == direction {
			return true
		}
	}
	return false
}
