package kamisado

var (
	A1      = (0x70)
	A2      = (0x60)
	A3      = (0x50)
	A4      = (0x40)
	A5      = (0x30)
	A6      = (0x20)
	A7      = (0x10)
	A8      = (0x00)
	B1      = (0x71)
	B2      = (0x61)
	B3      = (0x51)
	B4      = (0x41)
	B5      = (0x31)
	B6      = (0x21)
	B7      = (0x11)
	B8      = (0x01)
	C1      = (0x72)
	C2      = (0x62)
	C3      = (0x52)
	C4      = (0x42)
	C5      = (0x32)
	C6      = (0x22)
	C7      = (0x12)
	C8      = (0x02)
	D1      = (0x73)
	D2      = (0x63)
	D3      = (0x53)
	D4      = (0x43)
	D5      = (0x33)
	D6      = (0x23)
	D7      = (0x13)
	D8      = (0x03)
	E1      = (0x74)
	E2      = (0x64)
	E3      = (0x54)
	E4      = (0x44)
	E5      = (0x34)
	E6      = (0x24)
	E7      = (0x14)
	E8      = (0x04)
	F1      = (0x75)
	F2      = (0x65)
	F3      = (0x55)
	F4      = (0x45)
	F5      = (0x35)
	F6      = (0x25)
	F7      = (0x15)
	F8      = (0x05)
	G1      = (0x76)
	G2      = (0x66)
	G3      = (0x56)
	G4      = (0x46)
	G5      = (0x36)
	G6      = (0x26)
	G7      = (0x16)
	G8      = (0x06)
	H1      = (0x77)
	H2      = (0x67)
	H3      = (0x57)
	H4      = (0x47)
	H5      = (0x37)
	H6      = (0x27)
	H7      = (0x17)
	H8      = (0x07)
	Invalid = (0x80)
)

var (
	SquareMap = map[int]string{
		0x00: "a8", 0x01: "b8", 0x02: "c8", 0x03: "d8", 0x04: "e8", 0x05: "f8", 0x06: "g8", 0x07: "h8",
		0x10: "a7", 0x11: "b7", 0x12: "c7", 0x13: "d7", 0x14: "e7", 0x15: "f7", 0x16: "g7", 0x17: "h7",
		0x20: "a6", 0x21: "b6", 0x22: "c6", 0x23: "d6", 0x24: "e6", 0x25: "f6", 0x26: "g6", 0x27: "h6",
		0x30: "a5", 0x31: "b5", 0x32: "c5", 0x33: "d5", 0x34: "e5", 0x35: "f5", 0x36: "g5", 0x37: "h5",
		0x40: "a4", 0x41: "b4", 0x42: "c4", 0x43: "d4", 0x44: "e4", 0x45: "f4", 0x46: "g4", 0x47: "h4",
		0x50: "a3", 0x51: "b3", 0x52: "c3", 0x53: "d3", 0x54: "e3", 0x55: "f3", 0x56: "g3", 0x57: "h3",
		0x60: "a2", 0x61: "b2", 0x62: "c2", 0x63: "d2", 0x64: "e2", 0x65: "f2", 0x66: "g2", 0x67: "h2",
		0x70: "a1", 0x71: "b1", 0x72: "c1", 0x73: "d1", 0x74: "e1", 0x75: "f1", 0x76: "g1", 0x77: "h1",
	}

	SquareLookup = map[string]int{
		"a8": A8, "b8": B8, "c8": C8, "d8": D8, "e8": E8, "f8": F8, "g8": G8, "h8": H8,
		"a7": A7, "b7": B7, "c7": C7, "d7": D7, "e7": E7, "f7": F7, "g7": G7, "h7": H7,
		"a6": A6, "b6": B6, "c6": C6, "d6": D6, "e6": E6, "f6": F6, "g6": G6, "h6": H6,
		"a5": A5, "b5": B5, "c5": C5, "d5": D5, "e5": E5, "f5": F5, "g5": G5, "h5": H5,
		"a4": A4, "b4": B4, "c4": C4, "d4": D4, "e4": E4, "f4": F4, "g4": G4, "h4": H4,
		"a3": A3, "b3": B3, "c3": C3, "d3": D3, "e3": E3, "f3": F3, "g3": G3, "h3": H3,
		"a2": A2, "b2": B2, "c2": C2, "d2": D2, "e2": E2, "f2": F2, "g2": G2, "h2": H2,
		"a1": A1, "b1": B1, "c1": C1, "d1": D1, "e1": E1, "f1": F1, "g1": G1, "h1": H1,
	}

	board64square = []int{
		0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
		0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
		0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27,
		0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37,
		0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47,
		0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57,
		0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67,
		0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77,
	}

	BoardColorsSquare = map[int]int{
		A8: Orange, B8: Blue, C8: Purple, D8: Pink, E8: Yellow, F8: Red, G8: Green, H8: Maroon,
		A7: Red, B7: Orange, C7: Pink, D7: Green, E7: Blue, F7: Yellow, G7: Maroon, H7: Purple,
		A6: Green, B6: Pink, C6: Orange, D6: Red, E6: Purple, F6: Maroon, G6: Yellow, H6: Blue,
		A5: Pink, B5: Purple, C5: Blue, D5: Orange, E5: Maroon, F5: Green, G5: Red, H5: Yellow,
		A4: Yellow, B4: Red, C4: Green, D4: Maroon, E4: Orange, F4: Blue, G4: Purple, H4: Pink,
		A3: Blue, B3: Yellow, C3: Maroon, D3: Purple, E3: Red, F3: Orange, G3: Pink, H3: Green,
		A2: Purple, B2: Maroon, C2: Yellow, D2: Blue, E2: Green, F2: Pink, G2: Orange, H2: Red,
		A1: Maroon, B1: Green, C1: Red, D1: Yellow, E1: Pink, F1: Purple, G1: Blue, H1: Orange,
	}
)

func square(rank, file int) int {
	return (rank << 4) | file
}

func rank(square int) int {
	return square >> 4
}

func file(square int) int {
	return square & 15
}
