package kamisado

import "testing"
import "fmt"

func TestGivenBoardData_AsciiArtShouldBeCorrect(t *testing.T) {
	board := parseFEN("2prm3/b1o4R/YP1b1g2/2M1y3/2k2B2/G3K3/1O6/8 b 1")

	output := fmt.Sprintf(board.asciiArt())
	expectedBoard :=
		`
   +------------------------+
 8 | .  .  p  r  m  .  .  . |
 7 | b  .  o  .  .  .  .  R |
 6 | Y  P  .  b  .  g  .  . |
 5 | .  .  M  .  y  .  .  . |
 4 | .  .  k  .  .  B  .  . |
 3 | G  .  .  .  K  .  .  . |
 2 | .  O  .  .  .  .  .  . |
 1 | .  .  .  .  .  .  .  . |
   +------------------------+
     a  b  c  d  e  f  g  h
`
	if output != expectedBoard {
		t.Errorf("This is the expected board \n %s", output)
	}
}
