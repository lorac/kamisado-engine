package kamisado

import (
	"strconv"
	"strings"
)

const (
	DefaultFen = "obpkyrgm/8/8/8/8/8/8/MGRYKPBO b 1"
)

func parseFEN(fen string) *Board {

	board := NewBoard()
	parts := strings.Split(fen, " ")

	if len(parts) < 3 {
		return nil
	}

	// parts[0]: piece placement
	i := 0
	for j := 0; j < len(parts[0]); j++ {

		piece := string(parts[0][j])
		if piece == "/" {
			i += 8
		} else if p, err := strconv.ParseInt(piece, 10, 0); err == nil {
			i += int(p)
		} else {
			board.data[i] = stringPieceToInt(piece)
			i++
		}
	}

	// parts[1]: active color
	if parts[1] == "w" {
		board.turn = White
	} else {
		board.turn = Black
	}

	// parts[2]: fullmove clock
	if len(parts) >= 2 {
		fullMoves, _ := strconv.Atoi(parts[2])
		board.moveNumber = fullMoves
	}

	return board
}
